name := "scala-xrecords"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  "com.lihaoyi" % "ammonite" % "0.8.1" % "test" cross CrossVersion.full,
  "com.typesafe" % "config" % "1.3.1",
  "ch.qos.logback" % "logback-classic" % "1.1.7",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.lihaoyi" % "ammonite" % "0.9.9" % "test" cross CrossVersion.full
)

// For "doc" sbt command
//scalacOptions in Compile ++= Seq("-doc-root-content", "rootdoc.txt")

initialCommands in(Test, console) := """ammonite.Main().run()"""


# Polyglot xrecords implementation

This containing project groups 3 separate, stand-alone subprojects
implementing the [xrecords framework](https:/xrrocha.net/post/object-oriented-frameworks-1) in Java, Xtend and Scala.
